# Laravel UI - Vue (Typescript) & Tailwind CSS

This package pre-configures your fresh Laravel installation with Vue, Vue Router, Vuex and Tailwind CSS, using TypeScript formatting.
As Tailwind is a large package, Spatie's [PurgeCSS](https://github.com/spatie/laravel-mix-purgecss) is also implemented, to strip any 
unused CSS styling.

## Installation

```
composer require motomedialab/laravel-ui-vue-tailwind
php artisan ui vue-tailwind
```

Once the package has been installed, you need to install the npm packages and compile the assets for the first time.

```
npm install
npm run dev
```



## Extra

TypeScript allows for stricter control and better structure. For referencing on using the helpers packaged with
this script, please see the following sources - 

- https://blog.logrocket.com/how-to-write-a-vue-js-app-completely-in-typescript/
- https://github.com/championswimmer/vuex-module-decorators
- https://github.com/kaorun343/vue-property-decorator

### ToDos

- Configure authentication scaffolding 