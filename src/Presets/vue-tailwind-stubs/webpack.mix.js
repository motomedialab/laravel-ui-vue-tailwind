let
    mix = require('laravel-mix'),
    tailwindcss = require('tailwindcss'),
    purgeCss = require('laravel-mix-purgecss');


/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    .sass('resources/sass/app.scss', 'public/css')
    .ts('resources/js/app.ts', 'public/js')
    .options({
        processCssUrls: false,
        postCss: [tailwindcss('./tailwind.config.js')],
    })
    .webpackConfig(webpack => ({
        resolve: {
            alias: {
                "@": path.resolve(__dirname, 'resources')
            }
        },
        output: {
            // for ultimate cache blocking, include chunkhash
            // chunkFilename: "js/chunk/[name].[chunkhash].js",
            chunkFilename: "js/chunk/[name].js",
        }
    }));

if (mix.inProduction()) {
    mix
        .purgeCss()
        .version();
}
