import {VuexModule, Module, Mutation, Action} from 'vuex-module-decorators'

@Module({namespaced: true})
class Counter extends VuexModule {
  public count: number = 0;

  @Mutation
  public setCount(newCount: number): void {
    this.count = newCount;
  }

  @Action
  public increment(): void {
    this.context.commit('setCount', this.count + 1);
  }

  get currentCount(): number {
    return this.count;
  }
}

export default Counter
