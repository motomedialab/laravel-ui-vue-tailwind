<?php


namespace MotoMediaLab\LaravelUiVueTailwind\Presets;


use Laravel\Ui\Presets\Preset;

class VueTailwind extends Preset
{
    private static $stubsDir;

    public static function install()
    {
        static::$stubsDir = __DIR__ . '/vue-tailwind-stubs/';

        static::updateJs();
        static::updateSass();
        static::updateViews();
        static::updateRoutes();
        static::updatePackages();
        static::removeLegacyFiles();
        static::updateConfigurations();
    }

    protected static function updatePackageArray(array $packages): array
    {
        return [
            'vue' => '^2.6',
            'vuex' => '^3.1',
            'dotenv' => '^8.2',
            'vuex-class' => '^0.3',
            'vue-router' => '^3.1',
            'ts-loader' => '^6.2',
            'typescript' => '^3.8',
            'tailwindcss' => '^1.2',
            '@types/webpack-env' => '^1.15',
            'laravel-mix-purgecss' => '^4.1',
            'vue-template-compiler' => '^2.6',
            'vue-property-decorator' => '^8.4',
            'vuex-module-decorators' => '^0.16',
        ] + $packages;
    }

    protected static function updateJs()
    {
        static::copyDir(static::$stubsDir . 'resources/js', resource_path('js'));
    }

    protected static function updateViews()
    {
        copy(static::$stubsDir . '/resources/views/welcome.blade.php', resource_path('views/welcome.blade.php'));
    }

    protected static function updateSass(): void
    {
        copy(static::$stubsDir . 'resources/sass/app.scss', resource_path('sass/app.scss'));
    }

    protected static function updateRoutes(): void
    {
        copy(static::$stubsDir . 'routes/web.php', base_path('routes/web.php'));
    }

    protected static function updateConfigurations(): void
    {
        $configs = [
            'webpack.mix.js',
            'tailwind.config.js',
            'tsconfig.json',
            '.npmrc'
        ];

        foreach ($configs as $config) {
            copy(static::$stubsDir . $config, base_path($config));
        }
    }

    protected static function removeLegacyFiles()
    {
        $files = [
            'js/bootstrap.js'
        ];

        foreach ($files as $file) {
            if (file_exists(resource_path($file))) {
                unlink(resource_path($file));
            }
        }
    }

    private static function copyDir(string $directory, string $destination)
    {
        foreach (new \RecursiveDirectoryIterator($directory) as $file) {

            if (in_array($file->getFilename(), ['.', '..'])) {
                continue;
            }

            if ($file->isDir()) {
                $parts = explode('/', $file->getRealPath());
                $newDestination = $destination . '/' . end($parts);

                if (!is_dir($newDestination)) {
                    mkdir($newDestination);
                }

                static::copyDir($file->getRealPath(), $newDestination);
                continue;
            }

            copy($file->getRealPath(), $destination . '/' . $file->getFilename());
        }
    }


}