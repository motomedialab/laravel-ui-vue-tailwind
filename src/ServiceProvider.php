<?php

namespace MotoMediaLab\LaravelUiVueTailwind;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;
use Laravel\Ui\UiCommand;
use MotoMediaLab\LaravelUiVueTailwind\Presets\VueTailwind;

class ServiceProvider extends BaseServiceProvider
{
    
    public function register()
    {
        parent::register();

        UiCommand::macro('vue-tailwind', function () {
            VueTailwind::install();
        });
    }

}